*** Settings ***
Documentation    prendre un rdv et vérifier sa bonne prise
Library    Dialogs
Resource    ../Resources/Common.resource
Resource    ../Resources/PO/Accueil.resource
Resource    ../Resources/PO/Appointment.resource
Resource    ../Resources/PO/Confirmation.resource
Resource    ../Resources/PO/Login.resource

Test Setup         cura setup
Test Teardown      cura teardown

# avec le navigateur par defaut
# robot -d Results/cura_chromium -v VISIBLE:true Tests

*** Test Cases ***
make an appointment
    [Tags]  passant
    Given i am on cura healthcare page

    When click on make appointment

    Then i am on cura healthcare login page

    When enter good login credentials
    And click on login

    Then i am on cura healthcare form page

    When choose facility            ${FACILITY}
    And apply for readmission       ${APPLY_READMISSION}
    And select program              ${PROGRAM}
    And select date with pylib      ${DATE}
    And add a comment               ${COMMENT}

    And book appointment

    Then i am on cura healthcare confirmation page

    And check if informations are right    ${FACILITY}      ${APPLY_READMISSION}    ${PROGRAM}      ${DATE}     ${COMMENT}

    take screenshot

wrong credentials
    [Tags]  non passant

    i am on cura healthcare page

    click on make appointment

    i am on cura healthcare login page

    enter wrong login credentials
    click on login

    i have an error message



